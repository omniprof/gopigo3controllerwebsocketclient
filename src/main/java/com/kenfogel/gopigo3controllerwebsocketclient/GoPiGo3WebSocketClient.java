package com.kenfogel.gopigo3controllerwebsocketclient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omni_
 */
@ClientEndpoint
public class GoPiGo3WebSocketClient {
    
    private static final Logger LOG = LoggerFactory.getLogger(GoPiGo3WebSocketClient.class);
    
    public void peform() {
        String uri = "ws://192.168.10.101:8080/GoPiGo3ControllerWebSocketServer/server";
        WebSocketContainer container = ContainerProvider.
                getWebSocketContainer();
        try {
            container.connectToServer(this, new URI(uri));
        } catch (URISyntaxException | IOException | DeploymentException ex) {
            LOG.error(null, ex);
        }        
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        
        LOG.info("session opened");
        try {            
            session.getBasicRemote().sendText("ServoOn");
            Thread.sleep(5000);
            session.getBasicRemote().sendText("ServoOff");
            LOG.info("message sent successfully");
        } catch (IOException e) {
            LOG.error("ioexception error sending message to server", e);
        } catch (InterruptedException ex) {
            LOG.error("InterruptedException error sending message to server", ex);
        }        
    }

    /**
     *
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        LOG.info("Received: " + message);
    }
    
    public static void main(String[] args) {
        GoPiGo3WebSocketClient gepc = new GoPiGo3WebSocketClient();
        gepc.peform();
        System.exit(0);
    }
}
